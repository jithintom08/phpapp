FROM gliderlabs/herokuish
COPY . /app
ENV port=80
CMD ["/start", "web"]